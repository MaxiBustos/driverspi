#include "gain.h"

#define LMS_REG65 0x65

int setbbGain(int file, int bbGain){
	int status;

	if(bbGain >= 0 && bbGain <=30){
	status = spi_read(file,  LMS_REG65) & 0xE0;
	/* La ganancia solo acepta estos parametros
	 * {0,3,6,9,12,15,18,21,24,27,30}*/
	status = spi_write(file,  LMS_REG65, (status + (bbGain)/3));
	}
	else
		printf("Error");

	return status;
}


int getbbGain(int file){
	int status;

	status = (spi_read(file,  LMS_REG65) & 0x0000001F)*3;

	return status;
}

void getGainNames(void){
	printf("RXVGA2");
}

void getGainRange(void){
	printf("0 - 30 db");
}

void getGainMode(void){
	printf("RXVGA2");
}
