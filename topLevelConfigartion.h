#ifndef TOPLEVELCONFIGARTION_H_
#define TOPLEVELCONFIGARTION_H_

#include <stdint.h>
#include <unistd.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <fcntl.h>
#include <sys/ioctl.h>
#include <linux/types.h>
#include <linux/spi/spidev.h>
#include "SPIfunction.h"

/* @brief Setea Clock y ceierra el Switch.
 *
 * REG 0x09[7]:
 * 	RXOUTSW: RX out/ADC in high-Z switch control:
 * 		0 – switch open (RX output/ADC input chip pins disconnected) (default)
 * 		1 – switch closed, RXVGA2 should be powered off first <--- select
 * 	REG 0x09[2]: CLK_EN [6]:
 * 		1 – PLLCLKOUT enabled (default) <--- select
 * 		0 – PLLCLKOUT disabled
 * 	REG 0x09[2]: CLK_EN [2]:
 * 		1 – Rx DSM SPI clock enabled <--- select
 * 		0 – Rx DSM SPI clock disabled (default)
 *
 * @param file			Referencia al dispositivo SPI.
 *
 * @return				Estado de la operacion de escritura.
 */

int clockEnable  (int file);

#endif /* TOPLEVELCONFIGARTION_H_ */
