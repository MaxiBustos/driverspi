
#ifndef ANTENNA_H_
#define ANTENNA_H_

#include <stdint.h>
#include <unistd.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <getopt.h>
#include "SPIfunction.h"

/**
 *  @brief getAntennas: Devuelve las antenas activas.
 *
 * Funcion no implementada.
 *
 * @param
 *
 * @return
 */

int getAntennas(int file, int antenna);

/**
 *  @brief setAntenna: Selecciona LNA.
 *
 * Funcion que activa LNA correspondiente.
 * Esto se realiza seteando los bits 5 y 4 del registro 0x75 del LMS.
 *
 * LNASEL_RXFE[1:0]: Selects the active LNA.
 *			00 – all LNA's disabled
 *			01 – LNA1 active (default)
 *			10 – LNA2 active
 *			11 – LNA3 active
 *
 * A continuacion se setean los buffers correspondientes para el camino
 * de RX con los bits 1 y 0 del registro 0x25.
 *
 * SELOUT[1:0]: Select output buffer in RX PLL, not used in TX PLL
 *			00 – All output buffers powered down
 *			01 – First buffer enabled for LNA1 path (default)
 *			10 – Second buffer enabled for LNA2 path
 *			11 – Third buffer enabled for LNA3 path
 *
 * @param	file		Referencia al dispositivo SPI.
 * @param	antenna		Número de antenna a activar.
 *
 * @return  status		Estado de la operacion de escritura en registros.
 */

int setAntenna(int file, int antenna, int mode);

/**
 *  @brief getAntenna: Devuelve el camino LNA activo.
 *
 * Funcion que retorna cual camino de LNA esta activo.
 * Chequeando que los valores LNASEL_RXFE[1:0] == SELOUT[1:0]
 *
 * Reg[5:4]= LNASEL_RXFE[1:0]: Selects the active LNA.
 *			00 – all LNA's disabled
 *			01 – LNA1 active (default)
 *			10 – LNA2 active
 *			11 – LNA3 active
 *
 * Reg[1:0]= SELOUT[1:0]: Select output buffer in RX PLL, not used in TX PLL
 *			00 – All output buffers powered down
 *			01 – First buffer enabled for LNA1 path (default)
 *			10 – Second buffer enabled for LNA2 path
 *			11 – Third buffer enabled for LNA3 path
 *
 * @param	file		Referencia al dispositivo SPI.
 * @param   mode		Mode = 0 configuracion para TX, Mode = 1 configuracion para RX.
 *
 * @return  status		LNA activo.
 */

int getAntenna(int file);

#endif /* ANTENNA_H_ */
