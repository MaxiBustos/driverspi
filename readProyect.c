#include "readProyect.h"

int readProyect (int file, char *nombreArchivo){
	int status =0;
	FILE *doc;
	char buff[8];
	int value =0;
	int reg =0;

	if ((doc = fopen(nombreArchivo,"r")) < 0)
	{
		printf("Failed to open the file.\n");
		exit(1);
		}
		while(fgets(buff, sizeof(buff), doc) != NULL){
			value = strtol(buff,NULL,16);
			reg = (value & 0x7F00)>>8;
			printf("reg: %x\n", reg);
			value = (value & 0x00FF);
			printf("value: %x\n", value);
			printf("----------------\n");
		    status = spi_write(file, reg, value);
	      }
fclose(doc);
return status;
}
