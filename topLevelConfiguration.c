#include "topLevelConfigartion.h"

#define LMS_REG5 0x05
#define LMS_REG9 0x09


int clockEnable  (int file){
	int status =0;
/*
 * REG 0x09[7]:
 * RXOUTSW: RX out/ADC in high-Z switch control:
 * 	0 – switch open (RX output/ADC input chip pins disconnected) (default)
 * 	1 – switch closed, RXVGA2 should be powered off first <--- select
 * REG 0x09[2]: CLK_EN [6]:
 * 	1 – PLLCLKOUT enabled (default) <--- select
 * 	0 – PLLCLKOUT disabled
 * REG 0x09[2]: CLK_EN [2]:
 * 	1 – Rx DSM SPI clock enabled <--- select
 * 	0 – Rx DSM SPI clock disabled (default)*/

	status = spi_read(file, LMS_REG9);
	spi_write(file, LMS_REG9, (status & 196));

return status;
}
