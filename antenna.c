#include "antenna.h"

#define LMS_REG25 0x25
#define LMS_REG75 0x75
#define LMS_REG44 0x44
#define PA_EN_BIT4 4
#define PA_EN_BIT3 3

int setAntenna(int file, int antenna, int mode){
	int status=0;

	if (mode == 0){
		switch (antenna) {
			case 0: /* all PA Disabled */
				spi_clear_bit(file, LMS_REG44, PA_EN_BIT4);
				spi_clear_bit(file, LMS_REG44, PA_EN_BIT3);
				status = 0;
				break;
			case 1: /* enabled for PA1 */
				spi_clear_bit(file, LMS_REG44, PA_EN_BIT4);
				spi_set_bit(file, LMS_REG44, PA_EN_BIT3);
				status = 1;
				break;
			case 2: /* enabled for PA2 */
				spi_set_bit(file, LMS_REG44, PA_EN_BIT4);
				spi_clear_bit(file, LMS_REG44, PA_EN_BIT3);
				status = 2;
				break;
			case 3: /* all PA Disabled */
				spi_clear_bit(file, LMS_REG44, PA_EN_BIT4);
				spi_clear_bit(file, LMS_REG44, PA_EN_BIT3);
				status = 0;
				break;
			default: /* enabled for PA1 */
				spi_clear_bit(file, LMS_REG44, PA_EN_BIT4);
				spi_set_bit(file, LMS_REG44, PA_EN_BIT3);
				status = 1;
				break;
		}

	}else{
	switch (antenna) {
			case 0: /* all LNA Disabled */
				status = spi_read(file, LMS_REG25);
				spi_write(file, LMS_REG25, status & 0xFC);
				status = spi_read(file, LMS_REG75);
				spi_write(file, LMS_REG75, status & 0xE7);
				break;
			case 1: /* enabled for LNA1 */
				status = spi_read(file, LMS_REG25);
				spi_write(file, LMS_REG25, status | 0x02);
				status = spi_read(file, LMS_REG75);
				spi_write(file, LMS_REG75, status | 0x08);
				break;
			case 2: /* enabled for LNA2 */
				status = spi_read(file, LMS_REG25);
				spi_write(file, LMS_REG25, status | 0x02);
				status = spi_read(file, LMS_REG75);
				spi_write(file, LMS_REG75, status | 0x08);
				break;
			case 3: /* enabled for LNA3 */
				status = spi_read(file, LMS_REG25);
				spi_write(file, LMS_REG25, status | 0x3);
				status = spi_read(file, LMS_REG75);
				spi_write(file, LMS_REG75, status | 0x08);
				break;
			default: /* enabled for LNA1 */
				status = spi_read(file, LMS_REG25);
				spi_write(file, LMS_REG25, status | 0x02);
				status = spi_read(file, LMS_REG75);
				spi_write(file, LMS_REG75, status | 0x18);
				break;
		}
	}
	return status;
}


int getAntenna(int file){
	int status =0;
	int buffer =0;
	int lnaPath =0;

	buffer = spi_read(file, LMS_REG25) & 0x03;
	lnaPath = (spi_read(file, LMS_REG75)& 0x30) >> 4;

	if(buffer == lnaPath){
		switch (buffer) {
					case 0: /* all LNA Disabled */
						status = 0;
						break;
					case 1: /* enabled for LNA1 */
						status = 1;
						break;
					case 2: /* enabled for LNA2 */
						status = 2;
						break;
					case 3: /* enabled for LNA3 */
						status = 3;
						break;
					default: /* enabled for LNA1 */
						status = 0;
						break;
				}

	}
	else {
			printf("ERROR: No coincide el buffer con el LNA seleccionado.\n");
			}
	return status;
}
