#include "bandwidth.h"

#define kHz(x) (x * 1000)
#define MHz(x) (x * 1000000)
#define GHz(x) (x * 1000000000)

#define LMS_REG54 0x54
#define LMS_REG34 0x34
#define BWC_LPF_MASK (0x0F<<2)

static const uint32_t uint_bandwidths[] = {
    MHz(28),
    MHz(20),
    MHz(14),
    MHz(12),
    MHz(10),
    kHz(8750),
    MHz(7),
    MHz(6),
    kHz(5500),
    MHz(5),
    kHz(3840),
    MHz(3),
    kHz(2750),
    kHz(2500),
    kHz(1750),
    kHz(1500)
};

typedef enum {
    BW_28MHz,       /**< 28MHz bandwidth, 14MHz LPF */
    BW_20MHz,       /**< 20MHz bandwidth, 10MHz LPF */
    BW_14MHz,       /**< 14MHz bandwidth, 7MHz LPF */
    BW_12MHz,       /**< 12MHz bandwidth, 6MHz LPF */
    BW_10MHz,       /**< 10MHz bandwidth, 5MHz LPF */
    BW_8p75MHz,     /**< 8.75MHz bandwidth, 4.375MHz LPF */
    BW_7MHz,        /**< 7MHz bandwidth, 3.5MHz LPF */
    BW_6MHz,        /**< 6MHz bandwidth, 3MHz LPF */
    BW_5p5MHz,      /**< 5.5MHz bandwidth, 2.75MHz LPF */
    BW_5MHz,        /**< 5MHz bandwidth, 2.5MHz LPF */
    BW_3p84MHz,     /**< 3.84MHz bandwidth, 1.92MHz LPF */
    BW_3MHz,        /**< 3MHz bandwidth, 1.5MHz LPF */
    BW_2p75MHz,     /**< 2.75MHz bandwidth, 1.375MHz LPF */
    BW_2p5MHz,      /**< 2.5MHz bandwidth, 1.25MHz LPF */
    BW_1p75MHz,     /**< 1.75MHz bandwidth, 0.875MHz LPF */
    BW_1p5MHz,      /**< 1.5MHz bandwidth, 0.75MHz LPF */
} bw;


bw normalize_bw(uint32_t req)
{
    bw ret;
    if (     req <= kHz(1500)) ret = BW_1p5MHz;
    else if (req <= kHz(1750)) ret = BW_1p75MHz;
    else if (req <= kHz(2500)) ret = BW_2p5MHz;
    else if (req <= kHz(2750)) ret = BW_2p75MHz;
    else if (req <= MHz(3)  )  ret = BW_3MHz;
    else if (req <= kHz(3840)) ret = BW_3p84MHz;
    else if (req <= MHz(5)  )  ret = BW_5MHz;
    else if (req <= kHz(5500)) ret = BW_5p5MHz;
    else if (req <= MHz(6)  )  ret = BW_6MHz;
    else if (req <= MHz(7)  )  ret = BW_7MHz;
    else if (req <= kHz(8750)) ret = BW_8p75MHz;
    else if (req <= MHz(10) )  ret = BW_10MHz;
    else if (req <= MHz(12) )  ret = BW_12MHz;
    else if (req <= MHz(14) )  ret = BW_14MHz;
    else if (req <= MHz(20) )  ret = BW_20MHz;
    else                       ret = BW_28MHz;
    return ret;
}

int setBandwidth(int file, int bandwidth, int mode){
	bw normalized_bw;
	int data =0;
	int regLSB, regLPF =0;

	if(mode == 0){
		regLPF = LMS_REG34;
	}else{
		regLPF = LMS_REG54;
	}

	normalized_bw=normalize_bw(bandwidth);
	data = spi_read(file, regLPF);
	regLSB = data & 0x03;
	spi_write(file, regLPF, regLSB);
	data = spi_read(file, regLPF);
	data = (data | (normalized_bw<<2));
	spi_write(file, regLPF, data);
	return normalized_bw;
}

uint32_t getBandwidth(int file, int mode){
	bw normalized_bw;
	if (mode == 0){
		normalized_bw= spi_read(file, LMS_REG34);
		normalized_bw= (normalized_bw & BWC_LPF_MASK) >> 2;
		return uint_bandwidths[normalized_bw];
	}else{
		normalized_bw= spi_read(file, LMS_REG54);
		normalized_bw= (normalized_bw & BWC_LPF_MASK) >> 2;
		return uint_bandwidths[normalized_bw];
	}
}

void getBandwidthRange (void){
	printf("0.75 - 14 MHz");
}
