# Declaration of variables
CC = $(CROSS_COMPILE)gcc
CC_FLAGS = -Wall -O3 -lm
 
# File names
EXEC = spi
SOURCES = $(wildcard *.c)
OBJECTS = $(SOURCES:.c=.o)
 
# Main target
$(EXEC): $(OBJECTS)
	$(CC) $(OBJECTS) -o $(EXEC)
 
# To obtain object files
%.o: %.c
	$(CC) -c $(CC_FLAGS) $< -o $@
 
# To remove generated files
clean:
	rm -f $(EXEC) $(OBJECTS)

doc:
	doxygen doc/spidoc