#include "CentreFrequency.h"

/**
 *  @brief Registros del PLL TX
 *
 */

#define TXreg10 0x10
#define TXreg11 0x11
#define TXreg12 0x12
#define TXreg13 0x13
#define TXreg14 0x14
#define TXreg15 0x15
#define TXreg17 0x17
#define TXreg18 0x18
#define TXreg19 0x19
#define TXreg1A 0x1A


/**
 *  @brief Registros del PLL RX
 *
 */

#define RXreg20 0x20
#define RXreg21 0x21
#define RXreg22 0x22
#define RXreg23 0x23
#define RXreg24 0x24
#define RXreg25 0x25
#define RXreg27 0x27
#define RXreg28 0x28
#define RXreg29 0x29
#define RXreg2A 0x2A

#define Vtune_Desired 1.5
#define LMS_REG5  0x05
#define SRXEN_BIT 2
#define STXEN_BIT 3

int setCentreFreq (int file, float centreFreq, int mode){
	int status =0;
	int freqsel =0;
	int selOut =0;
	int regSELVCOFRANGE =0;
	struct divisors *struct_nint_nfrac;

	if (mode == 0){
		spi_set_bit(file, LMS_REG5, STXEN_BIT); //Enciende TX
		regSELVCOFRANGE = TXreg15;

	}else{
		spi_set_bit(file, LMS_REG5, SRXEN_BIT); //Enciende RX
		regSELVCOFRANGE = RXreg25;
	}

	freqsel = tableFreqsel(centreFreq);  /* freqsel:[selvco|frange] */

	if (freqsel > 0){

		selOut = getAntenna(file);

		status = spi_write(file, regSELVCOFRANGE, ((freqsel<<2) | selOut));/*Escritura del Reg 15TX | 25RX: [selvco|frange|selout]*/

		struct_nint_nfrac = nint_nfrac(freqsel & 0x07, centreFreq);

		status = setNINT_NFRAC(file, struct_nint_nfrac, mode); /* Sintonia "gruesa" */

		if (status >0)	/*Seteo de NINT y NFRAC correcto*/
			status = capSelProcedure(file, mode); /* Sintonia "fina" */
		else
			status = 0; /* Error VCO */
	}
	else{
		printf("Error set Centre freq\n");
		}

	return status;
}

int tableFreqsel(float centreFreq) {
	int selVco_Frange =0;

	(centreFreq >= 0.2325 && centreFreq < 0.285625) ? selVco_Frange = 39 :
	(centreFreq >= 0.285625 && centreFreq < 0.336875) ? selVco_Frange = 47 :
	(centreFreq >= 0.336875 && centreFreq < 0.405) ? selVco_Frange = 55 :
	(centreFreq >= 0.405 && centreFreq < 0.465) ? selVco_Frange = 63 :
	(centreFreq >= 0.465 && centreFreq < 0.57125) ? selVco_Frange = 38 :
	(centreFreq >= 0.57125 && centreFreq < 0.67375) ? selVco_Frange = 46 :
	(centreFreq >= 0.67375 && centreFreq < 0.81) ? selVco_Frange = 54 :
	(centreFreq >= 0.81 && centreFreq < 0.93) ? selVco_Frange = 62 :
	(centreFreq >= 0.93 && centreFreq < 1.1425) ? selVco_Frange = 37 :
	(centreFreq >= 1.1425 && centreFreq < 1.3475) ? selVco_Frange = 45 :
	(centreFreq >= 1.3475 && centreFreq < 1.62) ? selVco_Frange = 53 :
	(centreFreq >= 1.62 && centreFreq < 1.86) ? selVco_Frange = 61 :
	(centreFreq >= 1.86 && centreFreq < 2.285) ? selVco_Frange = 36 :
	(centreFreq >= 2.285 && centreFreq < 2.695) ? selVco_Frange = 44 :
	(centreFreq >= 2.695 && centreFreq < 3.24) ? selVco_Frange = 52 :
	(centreFreq >= 3.24 && centreFreq < 3.72) ? selVco_Frange = 60 : 0;

	return selVco_Frange;
}

struct divisors* nint_nfrac(int freqsel, float centreFreq) {

	int nint_aux =0;
	int nfrac_aux =0;
	int x =0;
	struct divisors *p;
	uint8_t valuesX[] = { 0, 2, 4, 8, 16 };

	freqsel = freqsel - 3;
	x = valuesX[freqsel];

	nint_aux = (x * (centreFreq * 1000)) / 30.72;

	nfrac_aux = 8388608 * (((x * centreFreq * 1000) / 30.72) - nint_aux);

	p = (struct divisors *) malloc(sizeof(struct divisors));
	p->nint = nint_aux;
	p->nfrac = nfrac_aux;

	return p;
}

int setNINT_NFRAC(int file, struct divisors * struct_nint_nfrac, int mode) {
	int status = 0;
	int nintMSB =0;
	int	nfrac22_16, nfrac15_8, nfrac7_0 =0;

	if (mode == 0){
		nintMSB	 	=   TXreg10;
		nfrac22_16 	=   TXreg11;
		nfrac15_8 	=   TXreg12;
		nfrac7_0 	=   TXreg13;

	}else{
		nintMSB 	=   RXreg20;
		nfrac22_16 	=   RXreg21;
		nfrac15_8 	=   RXreg22;
		nfrac7_0 	=   RXreg23;
	}

	/*Escritura de NINT  */
	/*reg 0x10h NINT[8:1]: Integer part of the divider (MSBs)  for TX */
	/*reg 0x20h NINT[8:1]: Integer part of the divider (MSBs)  for RX */
	spi_write(file, nintMSB, (struct_nint_nfrac->nint) >> 1);

	/*Escritura de NFRAC[22:16] y NINT[0] */
	/*reg 0x11h bit [7] -> NINT[0]: Integer part of the divider (LSB) for TX*/
	/*reg 0x11h bit [6:0] -> NFRAC[22:16]: Fractional part of the divide for TX*/

	/*reg 0x21h bit [7] -> NINT[0]: Integer part of the divider (LSB) for RX*/
	/*reg 0x21h bit [6:0] -> NFRAC[22:16]: Fractional part of the divide for RX*/
	if ((struct_nint_nfrac->nint) % 2 == 0) {
		status = spi_write(file, nfrac22_16,(struct_nint_nfrac->nfrac & 0x007F0000) >> 16);
	} else
		status = spi_write(file, nfrac22_16, ((struct_nint_nfrac->nfrac) >> 16) + 128);

	/*reg 0x12h NFRAC[15:8] for TX*/
	/*reg 0x22h NFRAC[15:8] for RX*/
	status = spi_write(file, nfrac15_8, (struct_nint_nfrac->nfrac & 0x0000FF00) >> 8);

	/*reg 0x13h NFRAC[7:0] for TX*/
	/*reg 0x23h NFRAC[7:0] for RX*/
	status = spi_write(file, nfrac7_0, (struct_nint_nfrac->nfrac & 0x000000FF));

	return status;
}

int capSelProcedure(int file, int mode) {
	struct varRegAlgoritmo *p;
	float formula = 0;
	int chargeCurrent, vcocapReg, vtuneReg =0;
	p = (struct varRegAlgoritmo *) malloc(sizeof(struct varRegAlgoritmo));

	if (mode == 0){
		chargeCurrent =   TXreg17;
		vcocapReg =   TXreg19;
		vtuneReg =   TXreg1A;
	}else{
		chargeCurrent =   RXreg27;
		vcocapReg =   RXreg29;
		vtuneReg =   RXreg2A;
	}
	/* Frist Step:
	 * Charge pump current Icp=1200uA (default)
	 * Charge pump current offset up Ioff up = 30uA.
	 * Charge pump current offset down Ioff down = 0uA (default)*/
	spi_write(file, chargeCurrent, 227);

	/*Strat Algoritmo*/
	/*RX: vcocap = 31*/
	p->data = spi_read(file, vcocapReg);
	p->data = p->data & 0x00000080; //mask para conservar VOVCOREG[0]: VCO regulator output voltage control, LSB
	p->data = p->data + 31;
	spi_write(file, vcocapReg, p->data);
	p->vcocap = spi_read(file, vcocapReg);
	/* vtune[....vtune_h vtune_l]*/
	usleep(500);
	p->vtune = (spi_read(file, vtuneReg));
	p->vtune = (spi_read(file, vtuneReg)) >> 6;
	switch (p->vtune) {
	case 0:
		conditionFirst(file, p, mode);
		break;
	case 1:
		conditionSecond(file, p, mode);
		break;
	case 2:
		conditionThird(file, p, mode);
		break;
	default:
		p->vcocap = 0;
		break;
	}


	/* PLL::VCOCAP := round(((Vtune_Desired-2.5)*(indCH-indCL)/(2.5-0.5))+indCH) */
	formula =round((((Vtune_Desired - 2.5) * (p->indCH - p->indCL)) / 2) + p->indCH);
	p->vcocap = ((spi_read(file, vcocapReg) & 0xC0) + formula);
	spi_write(file, vcocapReg, p->vcocap); //Carga valor de VCOCAP
	p->vcocap = spi_read(file, vcocapReg);
	return p->vcocap;
}

void conditionFirst(int file, struct varRegAlgoritmo *ptr, int mode) {

	int data_ = 0;
	data_ = ptr->data;

	/*Decrement PLL::VCOCAP
	 while (PLL::VTUNE_H <> 1
	 and PLL::VTUNE_L <> 0)
	 and PLL::VCOCAP > 0 */
	ptr->indCL = decrementCountONE(file, data_, mode);

	/*Increment PLL::VCOCAP
	 while (PLL::VTUNE_H <> 0
	 and PLL::VTUNE_L <> 1)
	 and PLL::VCOCAP < 63 */
	ptr->indCH = incrementCountONE(file, data_, mode);
}

void conditionSecond(int file, struct varRegAlgoritmo *ptr, int mode) {

	/*Decrement PLL::VCOCAP
	 while (PLL::VTUNE_H <> 0
	 and PLL::VTUNE_L <> 0) */
	ptr->indCL = decrementCountTWO(file, ptr->data, mode);

	/*Decrement PLL::VCOCAP
	 while (PLL::VTUNE_H <> 0
	 and PLL::VTUNE_L <> 0) */
	ptr->indCH = decrementCountONE(file, ptr->data, mode);

}

void conditionThird(int file, struct varRegAlgoritmo *ptr, int mode) {

	/*Decrement PLL::VCOCAP
	 while (PLL::VTUNE_H <> 0
	 and PLL::VTUNE_L <> 0) */
	ptr->indCL = incrementCountTHREE(file, ptr->data, mode);

	/*Decrement PLL::VCOCAP
	 while (PLL::VTUNE_H <> 0
	 and PLL::VTUNE_L <> 0) */
	ptr->indCH = incrementCountONE(file, ptr->data, mode);

}

int decrementCountONE(int file, int data, int mode) {
	int vtune_;
	int vcocap_;
	int vcocapReg =0;
	int vtuneReg =0;

	if (mode == 0){
			vcocapReg =   TXreg19;
			vtuneReg  =   TXreg1A;
		}else{
			vcocapReg =   RXreg29;
			vtuneReg  =   RXreg2A;
		}

	do {
			data = data - 1;
			spi_write(file, vcocapReg, data);
			usleep(500);
			vtune_ = spi_read(file, vtuneReg)>>6;
			vcocap_ = (spi_read(file, vcocapReg) & 0x0000003F);
		}while ((vtune_ != 2) && (vcocap_ >0));
return vcocap_;
}

int incrementCountONE(int file, int data, int mode) {
	int vtune_, vcocap_;
	int vcocapReg =0;
	int vtuneReg =0;

	if (mode == 0){
				vcocapReg =   TXreg19;
				vtuneReg  =   TXreg1A;
			}else{
				vcocapReg =   RXreg29;
				vtuneReg  =   RXreg2A;
			}
	do {
		data = data + 1;
		spi_write(file, vcocapReg, data);
		usleep(500);
		vtune_ = spi_read(file, vtuneReg) >> 6;
		vcocap_ = (spi_read(file, vcocapReg) & 0x0000003F);
	} while ((vtune_ != 1) && (vcocap_ < 63));
return vcocap_;
}


int decrementCountTWO(int file, int data, int mode) {
	int vtune_, vcocap_;
	int vcocapReg =0;
	int vtuneReg =0;

	if (mode == 0){
				vcocapReg =   TXreg19;
				vtuneReg  =   TXreg1A;
	}else{
		vcocapReg =   RXreg29;
		vtuneReg  =   RXreg2A;
	}
	do {
		data = data - 1;
		spi_write(file, vcocapReg, data);
		usleep(500);
		vtune_ = spi_read(file, vtuneReg) >> 6;
		vcocap_ = (spi_read(file, vcocapReg) & 0x0000003F);
	} while (vtune_ != 0);
	return vcocap_;
}

int incrementCountTHREE(int file, int data, int mode) {
	int vtune_, vcocap_;
	int vcocapReg =0;
	int vtuneReg =0;

	if (mode == 0){
				vcocapReg =   TXreg19;
				vtuneReg  =   TXreg1A;
			}else{
				vcocapReg =   RXreg29;
				vtuneReg  =   RXreg2A;
			}

	do {
		data = data - 1;
		spi_write(file, vcocapReg, data);
		usleep(500);
		vtune_ = spi_read(file, vtuneReg) >> 6;
		vcocap_ = (spi_read(file, vcocapReg) & 0x0000003F);
	} while (vtune_ != 0);
	return vcocap_;
}

float getCentreFreq (int file, int mode)
{
	float freq = 0;
	int nint_=0, nfrac_, x =0;
	uint8_t valuesX[] = { 0, 2, 4, 8, 16 };
	int	nfrac22_16, nfrac15_8, nfrac7_0 =0;
	int nintMSB, frange =0;

	if (mode == 0){
		nintMSB	 	=   TXreg10;
		nfrac22_16 	=   TXreg11;
		nfrac15_8 	=   TXreg12;
		nfrac7_0 	=   TXreg13;
		frange		=	TXreg15;

	}else{
		nintMSB 	=   RXreg20;
		nfrac22_16 	=   RXreg21;
		nfrac15_8 	=   RXreg22;
		nfrac7_0 	=   RXreg23;
		frange		=	RXreg25;
	}

	x = ((spi_read(file, frange) & 0x1C)>>2);
	x = x - 3;
	x = valuesX[x];

	nint_  = ((spi_read(file,nintMSB))<<1) + ((spi_read(file, nfrac22_16)&0x00000080)>>7);

	nfrac_ = ((spi_read(file,nfrac22_16)&0x7F)<<16) + ((spi_read(file, nfrac15_8))<<8) + (spi_read(file, nfrac7_0));

	freq = ((((float) nfrac_ / 8388608) + nint_)*30.72/1000)/x;

   return freq;
}

void getFreqRange(void){
	printf(" 0.3 - 3.8 GHz");
}
