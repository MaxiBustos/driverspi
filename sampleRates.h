/*
 * sampleRates.h
 *
 *  Created on: Jun 12, 2017
 *      Author: maxi
 */

#ifndef SAMPLERATES_H_
#define SAMPLERATES_H_

#include <stdint.h>
#include <unistd.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <getopt.h>
#include "SPIfunction.h"

/**
 *  @brief Retorna los valores de sample rates.
 *
 * Funcion no implementada.
 *
 * @param
 *
 * @return
 */

void getSampleRates (void);

/**
 *  @brief Establece el Sample Rate.
 *
 * Funcion no implementada. No se puede controlar el clock de la
 * EVB, salvo soldar las resistencias de 0 ohm correspondientes.
 *
 * @param
 *
 * @return
 */

void setSampleRate  (void);

/**
 *  @brief Retorna el clock de los ADC.
 *
 * Funcion que devuelve el valor de velocidad de sample de los ADC.
 * Por ahora el valor es fijo igual a 30.72 MHz.
 * @param
 *
 * @return				Valor fijo de clock. 30.72
 */

float getSampleRate  (void);

#endif /* SAMPLERATES_H_ */
