#ifndef CENTREFREQUENCY_H_
#define CENTREFREQUENCY_H_

#include <stdint.h>
#include <unistd.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <getopt.h>
#include <fcntl.h>
#include <math.h>
#include <sys/ioctl.h>
#include <linux/types.h>
#include <linux/spi/spidev.h>
#include "SPIfunction.h"
#include "topLevelConfigartion.h"
#include "antenna.h"

/**
 *  @brief Valores obtenidos para el pll
 *
 *	NINT: parte entera.
 *
 *	NFRAC: parte fraccional.
 *
 *	x: factor. \f$x=2^{(freqsel[2:0]-3)}\f$.
 *
 */

struct divisors {
	int nint;
	int nfrac;
};

/**
 *  @brief Estructura para guardar los valores de correspondiente
 * al algoritmo de seleccion de VCO.
 *
 * 	data: valor para cuenta.
 * 	vcocap: valor para cargar registro vco.
 * 	vtune: valor para la condicion
 *
 * 	Ver algoritmo de la pagina 29 del pdf
 * 	Programming and Calibration Guide 3.4.3 VCO Capacitor, Icp and Ioff Selection.
 */

struct varRegAlgoritmo {
	int data;
	int vcocap;
	int vtune;
	int indCL;
	int indCH;
};

/**
 *  @brief Establece la frecuencia Central.
 *
 * Funcion principal que llama a todos los demas metodos para lograr establecer la frecuencia central
 * solicitada.
 *
 *	-	Pasos:
 *		-# Enciende el transmisor RX por software.
 *		-# Devuelve valores fragne y selvco.
 *		-# Escribe los registros fragne y selvco.
 *		-# Calcula NINT y NFRAC y los setea.
 *		-# Realiza el algortimo para obtener el valor de VCO y escribirlo.
 *
 * @param file			Referencia al dispositivo SPI
 * @param centreFreq	Valor de la frecuencia central deseada
 * @param SelOut		Select output buffer in RX PLL, First buffer enabled for LNA1 path
 *
 * @return				Valor elegido para setear el VCOCAP
 */

int setCentreFreq		(int file, float centreFreq, int mode);

/**
 *  @brief Tabla con los valores de frange y selvco.
 *
 * Se devuelen los valores de los registros frange y selvco listos para poder setearlos en
 * el registro 0x25.
 *
 * @param file			Referencia al dispositivo SPI
 * @param centreFreq	Valor de la frecuencia central deseada
 * @param mode			mode = 0 configurar TXPLL, mode = 1 configurar RXPLL
 *
 * @return				Valores de frange y selvco.
 */


int tableFreqsel  (float centreFreq);

/**
 *  @brief Calculo de los vaores enteros y fraccionales para el pll.
 *
 * Con los tres bits mas significativo de freqsel se calcula x. Luego
 *
 * NINT: \f$\frac{x * freqDeseada}{freqRef}\f$
 *
 * NFRAC: \f$2^{23}(\frac{x * freqDeseada}{freqRef}  - NINT)\f$
 *
 *
 *
 * @param freqsel		Valor de frange y selvco.
 * @param centreFreq	Valor de la frecuencia central deseada.
 *
 * @return				puntero a la estructura de los valores NINT y NFRAC.
 */

struct divisors* nint_nfrac (int freqsel, float centreFreq);

/**
 *  @brief Escribe los registros NINT y NFRAC.
 *
 *	-	El registro NINT[8:0]: Integer part of the divider.
 *		- Reg 0x20[7:0]: NINT[8:1]: Integer part of the divider (MSBs).
 *		- Reg 0x21[7]: NINT[0]:   Integer part of the divider (LSB).
 *	-	El registro NFRAC[22:0]: Fractional part of the divider.
 *		- Reg 0x21[6:0]: NFRAC[22:16]
 *		- Reg 0x22[7:0]: NFRAC[15:8]
 *		- Reg 0x23[7:0]: NFRAC[7:0]
 *
 * @param file				Referencia al dispositivo SPI
 * @param struct_nint_nfrac	Puntero de la estructura con los valores nint y nfrac.
 *
 * @return					Estado de la operación de escritura de los registros.
 */

int setNINT_NFRAC (int file, struct divisors * struct_nint_nfrac, int mode);

/**
 *  @brief Realiza el algoritmo para la seleccion del valor del VCO.
 *
 * Seccion de 3.4.3 - VCO Capacitor, Icp and Ioff Selection
 *
 * @param file				Referencia al dispositivo SPI
 *
 * @return					Valor del VCOCAP.
 */

int capSelProcedure     (int file, int mode);

/**
 *  @brief Procedimiento para la condicion cuando VTUNE = 0.
 *
 * Se obtiene los valores indCL y indCH para el calculo de la formula
 * del algoritmo. Para guardarlos en la estructura.
 *
 * @param file	Referencia al dispositivo SPI
 * @param Puntero a la esructura.
 *
 * @return
 */

void conditionFirst		(int file, struct varRegAlgoritmo * p, int mode);

/**
 *  @brief Procedimiento para la condicion cuando VTUNE = 1.
 *
 * Se obtiene los valores indCL y indCH para el calculo de la formula
 * del algoritmo. Para guardarlos en la estructura.
 *
 * @param file	Referencia al dispositivo SPI
 * @param Puntero a la esructura.
 *
 * @return
 */

void conditionSecond	(int file, struct varRegAlgoritmo * p, int mode);

/**
 *  @brief Procedimiento para la condicion cuando VTUNE = 2.
 *
 * Se obtiene los valores indCL y indCH para el calculo de la formula
 * del algoritmo. Para guardarlos en la estructura.
 *
 * @param file	Referencia al dispositivo SPI
 * @param Puntero a la esructura.
 *
 * @return
 */

void conditionThird		(int file, struct varRegAlgoritmo * p, int mode);

/**
 *  @brief Decrement PLL::VCOCAP  while (PLL::VTUNE_H <> 1
 * and PLL::VTUNE_L <> 0)  and PLL::VCOCAP > 0
 *
 * Se obtiene los valores indCL y indCH para el calculo de la formula
 * del algoritmo. Para guardarlos en la estructura.
 *
 * @param file	Referencia al dispositivo SPI
 * @param Puntero a la esructura.
 *
 * @return
 */

int decrementCountONE   (int file, int data, int mode);

/**
 *  @brief Increment PLL::VCOCAP  while (PLL::VTUNE_H <> 1
 * and PLL::VTUNE_L <> 0)  and PLL::VCOCAP < 63
 *
 * Se obtiene el valor indCL
 *
 * @param 		file	Referencia al dispositivo SPI.
 * @param 		valor correspondiente para la cuenta.
 *
 * @return 		valor indCL.
 */

int incrementCountONE   (int file, int data, int mode);

/**
 *  @brief Decrement PLL::VCOCAP  while (PLL::VTUNE_H <> 1
 * and PLL::VTUNE_L <> 0)  and PLL::VCOCAP > 63
 *
 * Se obtiene el valor indCH
 *
 * @param 	file	Referencia al dispositivo SPI
 * @param 	valor correspondiente para la cuenta.
 *
 * @return valor indCL
 */

int decrementCountTWO   (int file, int data, int mode);

/**
 *  @brief Increment PLL::VCOCAP  while (PLL::VTUNE_H <> 1
 * and PLL::VTUNE_L <> 0)
 *
 * Se obtiene el valor indCL
 *
 * @param 		file	Referencia al dispositivo SPI.
 * @param 		valor correspondiente para la cuenta.
 *
 * @return 		valor indCL.
 */

int incrementCountTHREE (int file, int data, int mode);

/**
 *  @brief Devuelve el valor de la frecuencia central actual del pll.
 *
 * Se lee los valores NINT y NFRAC se obtiene la frecuencia central.
 *
 * freq = (((nfrac/pow(2,23)) + nint)*30,72)/x;
 *
 * @param 		file	Referencia al dispositivo SPI.
 * @param 		struct divisors para obtener valor de x.
 *
 * @return 		valor de la frecuancia central.
 */

float getCentreFreq (int file, int mode);

/**
 *  @brief Devuelve rango de frecuencia.
 *
 *	Rango de frecuencia del chip LMS6002.
 *
 * @param
 *
 * @return
 */

void getFreqRange(void);


#endif /* CENTREFREQUENCY_H_ */

