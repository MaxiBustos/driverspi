![logo.png](https://bitbucket.org/repo/5qdn9k5/images/2592136077-logo.png)


#SPI Driver

El kernel de Linux consiste en un cuerpo de código largo y complejo, basado en el sistema operativo UNIX. Los controladores de dispositivos poseen un rol espacial dentro del kernel de Linux.
Los controladores son "cajas negras" que permiten al usuario ocultar los detalles de comoo funcionan el dispositivo.  El usuario puede interactuar utilizando un conjunto de llamadas estandarizadas que son independientes del controlador específico.
El papel fundamental de los controladores de dispositivo es mapear estas llamadas a operaciones específicas propias del dispositivo, que actuan directamente sobre el hardware.
Estos permiten que los drivers sean diseñado separados del resto del kernel y utilizarlos en tiempo de ejecucion cuando sean necesarios. 

La placa de evaluación EVB-6002, contiene un chip transceptor LMS 6002, que permite tanto la transmicion y recepcion de señales, para ello acepta configuraciones de sus registros internos mediante spi. 
 
A fines de configurar el transceptor, fue desarrollado este driver que corre sobre una distribucion de GNU/Linux en una placa de desarollo de Terasic, cuyo modelo es DE4 y posee una fpga Stratix-IV.

###Imagen de Componentes de Hardware:

![Imagen Componentes.jpg](https://bitbucket.org/repo/5qdn9k5/images/2991708592-Imagen%20Componentes.jpg)


###Módulos de componentes configurables de la placa de evaluación 6002:

![functionalblockdiagramEVB.png](https://bitbucket.org/repo/5qdn9k5/images/1392395253-functionalblockdiagramEVB.png)

###Métodos utilizables por el driver SPI:

![spihelp](https://bitbucket.org/repo/5qdn9k5/images/3778640075-spihelp.png)

###Ejemplos de uso:

Configuración la frecuencia del pll y ancho de banda del filtro pasa bajo del módulo TX: 

```
#!bash

# ./spi -m tx -f 0.750 -b 10000000 
```

Obtención de la ganancia del amplificador VGA2 del módulo RX: 

```
#!bash

# ./spi -m rx -G  
```

Si se desean realizar cambios en el driver se necesitara acoplar el toolchain de Nios II al eclipse. La fuente se puede encontrar en el siguiente repositorio:

:arrow_right: https://github.com/mbats/eclipse-buildroot-bundle/wiki/Tutorial-:-How-to-activate-and-install-the-Buildroot-Eclipse-plugin-%3F

###Compilacion:

```
#!bash

# export CROSS_COMPILE=**TOOLCHAIN PATH**
# make
```

![alt tag](https://raw.githubusercontent.com/bustosperassi/niosii-spi/master/capturas/logoLCD.jpg)