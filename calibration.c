#include "calibration.h"

#define TRUE 1
#define FALSE 0
#define DC_START_CLBR_BIT 5
#define LPF_CAL_BIT 5
#define TOPSPI_BASE 0x00
#define RXLPF_BASE 0x50
#define TXLPF_BASE 0x30
#define RXVGA2_BASE 0x60
#define ADDR_0	0
#define ADDR_1	1
#define ADDR_2	2
#define ADDR_3	3
#define ADDR_4	4
#define LMS_REG0 0x00
#define LMS_REG2 0x02
#define LMS_REG5 0x05
#define LMS_REG6 0x06
#define LMS_REG9 0x09
#define LMS_REG35 0x35
#define LMS_REG36 0x36
#define LMS_REG55 0x55
#define LMS_REG56 0x56
#define LMS_REG7 0x07
#define LMS_REG1 0x01
#define TX_ENABLE_BIT 3
#define RX_ENABLE_BIT 2
#define TX_LPF_DCCAL_BIT 1
#define RX_LPF_DCCAL_BIT 3
#define RX_VGA2_DCCAL_BIT 4
#define CLKSEL_LPFCAL_BIT 3
#define PD_CLKLPFCAL_BIT 2
#define EN_CAL_LPFCAL_BIT 7
#define RST_CAL_LPFCAL_BIT 0
#define ReferenceClockFrequency 30.72

int generalDCalibration (int file, int base, int addr){
	int status =0;
	int dcAddr = base + 3;
	int dcRegVal =base;

	status = spi_read(file, dcAddr);
	status = status | addr;
	status = spi_write(file, dcAddr, status);
	spi_set_bit(file, dcAddr, DC_START_CLBR_BIT);
	spi_clear_bit(file, dcAddr, DC_START_CLBR_BIT);
	status = spi_read(file, dcRegVal);
	if(status !=31)
		return status = TRUE;
	else{
		spi_write(file, dcRegVal, 0);
		spi_set_bit(file, dcAddr, DC_START_CLBR_BIT);
		spi_clear_bit(file, dcAddr, DC_START_CLBR_BIT);
		status = spi_read(file, dcRegVal);
		if(status !=0)
			return status = TRUE;
		else
			return status = FALSE;
	}
}

int dcOffsetCalibrationLPFTuningModule (int file){
	int status =0;
	int LMS_REG9_CONTEX =0;

	LMS_REG9_CONTEX = spi_read(file, LMS_REG9);
	spi_set_bit(file, LMS_REG9, LPF_CAL_BIT);
	status = generalDCalibration(file, TOPSPI_BASE, ADDR_0);
	if (status == TRUE){
		status = spi_read(file, LMS_REG0);
		spi_write(file, LMS_REG2, status);
		spi_write(file, LMS_REG35, status);
		spi_write(file, LMS_REG55, status);
		status = TRUE;
	} else {
		printf("PANIC: Algorithm does Not Converge!\n");
		status = FALSE;
	}
	spi_write(file, LMS_REG9, LMS_REG9_CONTEX);

return status;
}

int lpfDCOffsetCalibration (int file, int mode, int base, int addr){
	int status =0;
	int LMS_REG9_CONTEX =0;

	LMS_REG9_CONTEX = spi_read(file, LMS_REG9);
	spi_set_bit(file, LMS_REG9, TX_LPF_DCCAL_BIT);
	spi_set_bit(file, LMS_REG9, RX_LPF_DCCAL_BIT);
	status = generalDCalibration(file, TOPSPI_BASE, ADDR_0);
	if (status == TRUE){
		if (mode == 0){ //RX
			if(generalDCalibration(file, RXLPF_BASE, ADDR_1))
				status = TRUE;
			else
				status = FALSE;
		}
		else if(mode == 1){ //TX
			if(generalDCalibration(file, TXLPF_BASE, ADDR_1))
				status = TRUE;
			else
				status = FALSE;
		}
	}else{
		status = FALSE;
	}
spi_write(file, LMS_REG9, LMS_REG9_CONTEX);
return status;
}

int rxVGA2DCOffsetCalibration (int file){
	int status =FALSE;
	int LMS_REG9_CONTEX =0;

		LMS_REG9_CONTEX = spi_read(file, LMS_REG9);
		spi_set_bit(file, LMS_REG9, RX_VGA2_DCCAL_BIT);
		if (generalDCalibration(file, RXVGA2_BASE, ADDR_1)){
			if (generalDCalibration(file, RXVGA2_BASE, ADDR_2))
				if (generalDCalibration(file, RXVGA2_BASE, ADDR_3))
					if (generalDCalibration(file, RXVGA2_BASE, ADDR_4))
						status = TRUE;
		}else
						status = FALSE;

	spi_write(file, LMS_REG9, LMS_REG9_CONTEX);
	return status;
}

int lpfBandwidthTuning (int file){
	int status =FALSE;
	unsigned int rccal =0;

	if(ReferenceClockFrequency == 40 ){
		/* Enable TxPLL and set to Produce 320MHz */
		status = setCentreFreq(file, 320, 0);
		/*Use 40MHz generated From TxPLL*/
		spi_clear_bit(file, LMS_REG6, CLKSEL_LPFCAL_BIT);
		/*Power Up LPF tuning clock generation block:*/
		spi_clear_bit(file, LMS_REG6, PD_CLKLPFCAL_BIT);
	}
	/*TopSPI::BWC_LPFCAL:=0x07 (2.5MHz, as example)*/
		status = spi_read(file, LMS_REG7);
		status |= 0x07;
		spi_write(file, LMS_REG7, 0x07);

	/*TopSPI::EN_CAL_LPFCAL :=1 (Enable)*/
		spi_set_bit(file, LMS_REG7, EN_CAL_LPFCAL_BIT);

	/*TopSPI::RST_CAL_LPFCAL:= 1 (Rst Active)*/
		spi_set_bit(file, LMS_REG6, RST_CAL_LPFCAL_BIT);
		usleep(3);
	/*TopSPI::RST_CAL_LPFCAL:= 0 (Rst Inactive)*/
		spi_clear_bit(file, LMS_REG6, RST_CAL_LPFCAL_BIT);

	/*RCCAL :=TopSPI::RCCAL_LPFCAL*/
		rccal = spi_read(file, LMS_REG1)>>5;

	/*RxLPFSPI::RCCAL_LPF :=RCCAL*/
		status = spi_read(file, LMS_REG56);
		status = status | (rccal<<4);
		status = spi_write(file, LMS_REG56, status);

	/*TxLPFSPI::RCCAL_LPF :=RCCAL*/
		status = spi_read(file, LMS_REG36);
		status = status | (rccal<<4);
		status = spi_write(file, LMS_REG36, status);

	return status;
}

int autoCalibration (int file){
	int status =0;

	spi_clear_bit(file, LMS_REG5, TX_ENABLE_BIT);
	spi_clear_bit(file, LMS_REG5, RX_ENABLE_BIT);

	if(dcOffsetCalibrationLPFTuningModule(file))
	{
		if(lpfBandwidthTuning(file))
		{
			if(lpfDCOffsetCalibration(file, 1, RXLPF_BASE, ADDR_1))
			{
				if(lpfDCOffsetCalibration(file, 0, TXLPF_BASE, ADDR_1))
				{
					if(rxVGA2DCOffsetCalibration(file))
					{
						 status = TRUE;
					}
					else
					{
						status = FALSE;
					}
				}
			}
		}
	}

	spi_set_bit(file, LMS_REG5, TX_ENABLE_BIT);
	spi_set_bit(file, LMS_REG5, RX_ENABLE_BIT);
	return status;

}
