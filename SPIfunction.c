#include "SPIfunction.h"

struct spi_ioc_transfer xfer[2];


int spi_init(char  * filename)
{
	int file;

        if ((file = open(filename,O_RDWR)) < 0)
        {
            printf("Failed to open the bus.\n");
   /* ERROR HANDLING; you can check errno to see what went wrong */
            exit(1);
            }


    //xfer[0].tx_buf = (unsigned long)buf;
    xfer[0].len = 3; /* Length of  command to write <- //revisar si estas condiciones estan de mas!!*/
    xfer[0].cs_change = 0; /* Keep CS activated */
    xfer[0].delay_usecs = 0, //delay in us
    xfer[0].speed_hz = 0, //speed
    xfer[0].bits_per_word = 0, // bites per word 8

    //xfer[1].rx_buf = (unsigned long) buf2;
    xfer[1].len = 4; /* Length of Data to read  <- //revisar si estas condiciones estan de mas!!*/
    xfer[1].cs_change = 0; /* Keep CS activated */
    xfer[0].delay_usecs = 0;
    xfer[0].speed_hz = 0;
    xfer[0].bits_per_word = 0;

    return file;
}

int spi_read(int file, int regRead)
{
	unsigned char   buf[2], buf2[2];
	int status;
	int data;

    memset(buf, 0, sizeof buf);
    memset(buf2, 0, sizeof buf2);

    buf[0] =regRead;
    xfer[0].tx_buf = (unsigned long)buf;
    xfer[0].len = 1; /* Length of  command to write*/
    xfer[1].rx_buf = (unsigned long) buf2;
    xfer[1].len = 1; /* Length of Data to read */
    status = ioctl(file, SPI_IOC_MESSAGE(2), xfer);
    if (status < 0)
        {
        perror("SPI_IOC_MESSAGE");
        exit (1);
        }
    data =(buf2[0]);
    return data;
}

int spi_write(int file, int regWrite, int valueWrite)
{
    char   buf[2];
    int status;

    memset(buf, 0, sizeof buf);
    buf[0] = 0x80 | regWrite;
    buf[1] =  valueWrite;
    xfer[0].tx_buf = (unsigned long)buf;
    xfer[0].len = 2; /* Length of  command to write*/
    status = ioctl(file, SPI_IOC_MESSAGE(1), xfer);
    if (status < 0)
        {
        perror("SPI_IOC_MESSAGE");
        }
    return status;
}

int spi_set_bit(int file,  int regWrite, int bit){
    int status =0;

    status = spi_read(file, regWrite);
    status |= (1<<bit);
    status = spi_write(file, regWrite, status);

  return status;
}

int spi_clear_bit(int file, int regWrite, int bit){
    int status =0;

    	status = spi_read(file, regWrite);
    	status &= ~(1 << bit);
        status = spi_write(file, regWrite, status);

  return status;
}
