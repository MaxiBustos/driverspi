var searchData=
[
  ['data',['data',['../structvarRegAlgoritmo.html#a50b5a5b928eab43982697895488b8fac',1,'varRegAlgoritmo']]],
  ['dc_5fstart_5fclbr_5fbit',['DC_START_CLBR_BIT',['../calibration_8c.html#aa86460a621906864f0979ad929a8b1b6',1,'calibration.c']]],
  ['dcoffsetcalibrationlpftuningmodule',['dcOffsetCalibrationLPFTuningModule',['../calibration_8c.html#ad4eb11315c4fbebabae3840856706b81',1,'dcOffsetCalibrationLPFTuningModule(int file):&#160;calibration.c'],['../calibration_8h.html#ad4eb11315c4fbebabae3840856706b81',1,'dcOffsetCalibrationLPFTuningModule(int file):&#160;calibration.c']]],
  ['decrementcountone',['decrementCountONE',['../CentreFrequency_8c.html#aeef1913b857e6ad6d19c094e46aa044e',1,'decrementCountONE(int file, int data, int mode):&#160;CentreFrequency.c'],['../CentreFrequency_8h.html#aeef1913b857e6ad6d19c094e46aa044e',1,'decrementCountONE(int file, int data, int mode):&#160;CentreFrequency.c']]],
  ['decrementcounttwo',['decrementCountTWO',['../CentreFrequency_8c.html#ad076273db8696df03ea03242820866e3',1,'decrementCountTWO(int file, int data, int mode):&#160;CentreFrequency.c'],['../CentreFrequency_8h.html#ad076273db8696df03ea03242820866e3',1,'decrementCountTWO(int file, int data, int mode):&#160;CentreFrequency.c']]],
  ['device',['device',['../parseOpts_8h.html#af2ff1e3088c734549b4c3676c5e86a22',1,'parseOpts.h']]],
  ['divisors',['divisors',['../structdivisors.html',1,'']]],
  ['dynsections_2ejs',['dynsections.js',['../dynsections_8js.html',1,'']]]
];
