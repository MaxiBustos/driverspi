var searchData=
[
  ['searchbox',['SearchBox',['../search_8js.html#a52066106482f8136aa9e0ec859e8188f',1,'search.js']]],
  ['searchresults',['SearchResults',['../search_8js.html#a9189b9f7a32b6bc78240f40348f7fe03',1,'search.js']]],
  ['setantenna',['setAntenna',['../antenna_8c.html#a4c71ee81deb44455aff1e7f4edaab832',1,'setAntenna(int file, int antenna, int mode):&#160;antenna.c'],['../antenna_8h.html#a4c71ee81deb44455aff1e7f4edaab832',1,'setAntenna(int file, int antenna, int mode):&#160;antenna.c']]],
  ['setbandwidth',['setBandwidth',['../bandwidth_8c.html#ab26fc6865e058e823e503e9ff765b2fa',1,'setBandwidth(int file, int bandwidth, int mode):&#160;bandwidth.c'],['../bandwidth_8h.html#ab26fc6865e058e823e503e9ff765b2fa',1,'setBandwidth(int file, int bandwidth, int mode):&#160;bandwidth.c']]],
  ['setbbgain',['setbbGain',['../gain_8c.html#a664f5a0083bdc10df5b20aa0a54ae29f',1,'setbbGain(int file, int bbGain):&#160;gain.c'],['../gain_8h.html#a664f5a0083bdc10df5b20aa0a54ae29f',1,'setbbGain(int file, int bbGain):&#160;gain.c']]],
  ['setcentrefreq',['setCentreFreq',['../CentreFrequency_8c.html#af64004417a27439e64c217fd3f8db2bb',1,'setCentreFreq(int file, float centreFreq, int mode):&#160;CentreFrequency.c'],['../CentreFrequency_8h.html#af64004417a27439e64c217fd3f8db2bb',1,'setCentreFreq(int file, float centreFreq, int mode):&#160;CentreFrequency.c']]],
  ['setclassattr',['setClassAttr',['../search_8js.html#a499422fc054a5278ae32801ec0082c56',1,'search.js']]],
  ['setgainmode',['setGainMode',['../gain_8h.html#a34fe8938db4986c6772f4529b249c1d1',1,'gain.h']]],
  ['setkeyactions',['setKeyActions',['../search_8js.html#a98192fa2929bb8e4b0a890a4909ab9b2',1,'search.js']]],
  ['setnint_5fnfrac',['setNINT_NFRAC',['../CentreFrequency_8c.html#a27c810f80a73ca2d63084bd0656aa3fa',1,'setNINT_NFRAC(int file, struct divisors *struct_nint_nfrac, int mode):&#160;CentreFrequency.c'],['../CentreFrequency_8h.html#a27c810f80a73ca2d63084bd0656aa3fa',1,'setNINT_NFRAC(int file, struct divisors *struct_nint_nfrac, int mode):&#160;CentreFrequency.c']]],
  ['setsamplerate',['setSampleRate',['../sampleRates_8h.html#a0eddea3c61fb067376b6bec924f45775',1,'sampleRates.h']]],
  ['spi_5fclear_5fbit',['spi_clear_bit',['../SPIfunction_8c.html#a53fd1e07cd70f15cd71cd01bedf5558e',1,'spi_clear_bit(int file, int regWrite, int bit):&#160;SPIfunction.c'],['../SPIfunction_8h.html#a53fd1e07cd70f15cd71cd01bedf5558e',1,'spi_clear_bit(int file, int regWrite, int bit):&#160;SPIfunction.c']]],
  ['spi_5finit',['spi_init',['../SPIfunction_8c.html#ad1d47416dc561b6a087e7f439a6c9e04',1,'spi_init(char *filename):&#160;SPIfunction.c'],['../SPIfunction_8h.html#ad1d47416dc561b6a087e7f439a6c9e04',1,'spi_init(char *filename):&#160;SPIfunction.c']]],
  ['spi_5fread',['spi_read',['../SPIfunction_8c.html#ae3b982070ee854cbb7fa4cd316be56a2',1,'spi_read(int file, int regRead):&#160;SPIfunction.c'],['../SPIfunction_8h.html#ae3b982070ee854cbb7fa4cd316be56a2',1,'spi_read(int file, int regRead):&#160;SPIfunction.c']]],
  ['spi_5fset_5fbit',['spi_set_bit',['../SPIfunction_8c.html#a93ca683b7c7a4e3c955e7349e1b15902',1,'spi_set_bit(int file, int regWrite, int bit):&#160;SPIfunction.c'],['../SPIfunction_8h.html#a93ca683b7c7a4e3c955e7349e1b15902',1,'spi_set_bit(int file, int regWrite, int bit):&#160;SPIfunction.c']]],
  ['spi_5fwrite',['spi_write',['../SPIfunction_8c.html#a57f4a02962d483121ade2ee9ec57c58f',1,'spi_write(int file, int regWrite, int valueWrite):&#160;SPIfunction.c'],['../SPIfunction_8h.html#a57f4a02962d483121ade2ee9ec57c58f',1,'spi_write(int file, int regWrite, int valueWrite):&#160;SPIfunction.c']]]
];
