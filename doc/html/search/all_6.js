var searchData=
[
  ['gain_2ec',['gain.c',['../gain_8c.html',1,'']]],
  ['gain_2eh',['gain.h',['../gain_8h.html',1,'']]],
  ['generaldcalibration',['generalDCalibration',['../calibration_8c.html#a1233e9e962a735606f3058aa7be68ed8',1,'generalDCalibration(int file, int base, int addr):&#160;calibration.c'],['../calibration_8h.html#a1233e9e962a735606f3058aa7be68ed8',1,'generalDCalibration(int file, int base, int addr):&#160;calibration.c']]],
  ['getantenna',['getAntenna',['../antenna_8c.html#a5b362d905929ca8aa1e4f9debe512603',1,'getAntenna(int file):&#160;antenna.c'],['../antenna_8h.html#a5b362d905929ca8aa1e4f9debe512603',1,'getAntenna(int file):&#160;antenna.c']]],
  ['getantennas',['getAntennas',['../antenna_8h.html#af7b108e439006244cd9cb1b2e188902a',1,'antenna.h']]],
  ['getbandwidth',['getBandwidth',['../bandwidth_8c.html#a8f56ec3dd1e329cb793c7df6ed4e4b9b',1,'getBandwidth(int file, int mode):&#160;bandwidth.c'],['../bandwidth_8h.html#a8f56ec3dd1e329cb793c7df6ed4e4b9b',1,'getBandwidth(int file, int mode):&#160;bandwidth.c']]],
  ['getbandwidthrange',['getBandwidthRange',['../bandwidth_8c.html#ab439035030f06668289add3ed4eab51a',1,'getBandwidthRange(void):&#160;bandwidth.c'],['../bandwidth_8h.html#ab439035030f06668289add3ed4eab51a',1,'getBandwidthRange(void):&#160;bandwidth.c']]],
  ['getbbgain',['getbbGain',['../gain_8c.html#a55eb510ed6e38182ee2adafae6cd6359',1,'getbbGain(int file):&#160;gain.c'],['../gain_8h.html#a55eb510ed6e38182ee2adafae6cd6359',1,'getbbGain(int file):&#160;gain.c']]],
  ['getcentrefreq',['getCentreFreq',['../CentreFrequency_8c.html#abdd6c66bcbef2b386b3520159348b318',1,'getCentreFreq(int file, int mode):&#160;CentreFrequency.c'],['../CentreFrequency_8h.html#abdd6c66bcbef2b386b3520159348b318',1,'getCentreFreq(int file, int mode):&#160;CentreFrequency.c']]],
  ['getfreqrange',['getFreqRange',['../CentreFrequency_8c.html#ad36f6e7b792b54b282851dadc5b1c53c',1,'getFreqRange(void):&#160;CentreFrequency.c'],['../CentreFrequency_8h.html#ad36f6e7b792b54b282851dadc5b1c53c',1,'getFreqRange(void):&#160;CentreFrequency.c']]],
  ['getgainmode',['getGainMode',['../gain_8c.html#a1e0c209f2ea8eba3aca2bb7dba8fe9f9',1,'getGainMode(void):&#160;gain.c'],['../gain_8h.html#a1e0c209f2ea8eba3aca2bb7dba8fe9f9',1,'getGainMode(void):&#160;gain.c']]],
  ['getgainnames',['getGainNames',['../gain_8c.html#a120ba36c881456f9480e8fb97d592be0',1,'getGainNames(void):&#160;gain.c'],['../gain_8h.html#a120ba36c881456f9480e8fb97d592be0',1,'getGainNames(void):&#160;gain.c']]],
  ['getgainrange',['getGainRange',['../gain_8c.html#ac667b369ba52d1d50b6d5705959c4a47',1,'getGainRange(void):&#160;gain.c'],['../gain_8h.html#ac667b369ba52d1d50b6d5705959c4a47',1,'getGainRange(void):&#160;gain.c']]],
  ['getsamplerate',['getSampleRate',['../sampleRates_8c.html#ad17fab8ce2bf7ef17f53e2e99e505407',1,'getSampleRate(void):&#160;sampleRates.c'],['../sampleRates_8h.html#ad17fab8ce2bf7ef17f53e2e99e505407',1,'getSampleRate(void):&#160;sampleRates.c']]],
  ['getsamplerates',['getSampleRates',['../sampleRates_8h.html#a3fd3fc5bc3adb4bdf6b3d4ed1e347f24',1,'sampleRates.h']]],
  ['getxpos',['getXPos',['../search_8js.html#a76d24aea0009f892f8ccc31d941c0a2b',1,'search.js']]],
  ['getypos',['getYPos',['../search_8js.html#a8d7b405228661d7b6216b6925d2b8a69',1,'search.js']]],
  ['ghz',['GHz',['../bandwidth_8c.html#ab6b840881fe7b740cc973b889f14ca53',1,'bandwidth.c']]]
];
