################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../CentreFrequency.c \
../SPIfunction.c \
../antenna.c \
../bandwidth.c \
../calibration.c \
../gain.c \
../main.c \
../parseOpts.c \
../readProyect.c \
../sampleRates.c \
../topLevelConfiguration.c 

O_SRCS += \
../CentreFrequency.o \
../SPIfunction.o \
../antenna.o \
../bandwidth.o \
../calibration.o \
../gain.o \
../main.o \
../parseOpts.o \
../readProyect.o \
../sampleRates.o \
../topLevelConfiguration.o 

OBJS += \
./CentreFrequency.o \
./SPIfunction.o \
./antenna.o \
./bandwidth.o \
./calibration.o \
./gain.o \
./main.o \
./parseOpts.o \
./readProyect.o \
./sampleRates.o \
./topLevelConfiguration.o 

C_DEPS += \
./CentreFrequency.d \
./SPIfunction.d \
./antenna.d \
./bandwidth.d \
./calibration.d \
./gain.d \
./main.d \
./parseOpts.d \
./readProyect.d \
./sampleRates.d \
./topLevelConfiguration.d 


# Each subdirectory must supply rules for building sources it contributes
%.o: ../%.c
	@echo 'Building file: $<'
	@echo 'Invoking: Buildroot NIOS2 C Compiler (/home/maxi/Downloads/buildroot/output)'
	/home/maxi/Downloads/buildroot/output/host/usr/bin/nios2-LCD_FCEFyN-linux-gnu-gcc -O0 -g3 -Wall -c -fmessage-length=0 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@:%.o=%.d)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


