
#ifndef SPIFUNCTION_H_
#define SPIFUNCTION_H_

#include <stdint.h>
#include <unistd.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <getopt.h>
#include <fcntl.h>
#include <sys/ioctl.h>
#include <linux/types.h>
#include <linux/spi/spidev.h>

/* @brief Iniciliza el dispisotivo SPI.
 *
 * Funcion que se encraga de inicilizar el dispositivo SPI para la comunicacion con los
 * registros de la EVB. Establece las condiciones necesarias y caracteristicas del
 * protocolo SPI.
 *
 * @param filename		Nombre del dispositivo SPI.
 *
 * @return				Valor del decriptor.
 */

int spi_init(char *filename);

/* @brief Lectura de registro SPI.
 *
 * Para leer un registro solo basta con poner el valor del
 * registro correspondiente en hexadecimal como lo indica
 * el datasheet. Por Ej: -r 0x04
 *
 * @param file		Nombre del dispositivo SPI.
 *
 * @return			Valor del registro.
 */

int spi_read(int file, int regRead);

/* @brief Escritura de registro SPI.
 *
 * Para escribir un registro es necesario colocar el número
 * del registro correspondiente en hexadecimal como lo indica
 * el datasheet acompañado por el valor que se desea setear en
 * dicho registro. Por Ej: -w 0x05 -v 52.
 *
 * @param file			Nombre del dispositivo SPI.
 * @param regWrite		Numero del registro a escribir.
 * @param valueWrite	Valor a escribir en el registro.
 *
 * @return				Estado de la operación.
 */

int spi_write(int file, int regWrite, int valueWrite);

/* @brief Activar un bit especifico del registro.
 *
 * @param file			Nombre del dispositivo SPI.
 * @param regWrite		Numero del registro a escribir.
 * @param bit			Numeo del bit que se quiere setear.
 *
 * @return				Estado de la operación.
 */

int spi_set_bit(int file, int regWrite, int bit);

/* @brief Limpiar un bit especifico del registro.
 *
 * @param file			Nombre del dispositivo SPI.
 * @param regWrite		Numero del registro a escribir.
 * @param bit			Numeo del bit que se quiere setear.
 *
 * @return				Estado de la operación.
 */

int spi_clear_bit(int file, int regWrite, int bit);

#endif /* SPIFUNCTION_H_ */
