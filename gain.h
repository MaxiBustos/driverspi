/*
 * gain.h
 *
 *  Created on: Jun 9, 2017
 *      Author: maxi
 */

#ifndef GAIN_H_
#define GAIN_H_

#include <stdint.h>
#include <unistd.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <fcntl.h>
#include <sys/ioctl.h>
#include <linux/types.h>
#include <linux/spi/spidev.h>
#include "SPIfunction.h"

/**
 *  @brief Establece la ganacia elegida.
 *
 * La funcion primero chequea que este dentro de las ganancias
 * recomendadas por el fabricante.
 *
 * Las ganancias que pueden setearse son de 0 a 30 dB, por un paso mínimo de 3 dB.
 *
 * @param file			Referencia al dispositivo SPI.
 * @param centreFreq	Valor de la ganancia deseada.
 * @param reg			registro al cual se le setea la ganancia.
 *
 * @return				Estado de escritua de registro.
 */

int setbbGain (int file, int bbGain);

/**
 *  @brief Retorna valor de ganancia del RXVGA2.
 *
 * Se obtiene el valor de la ganacia del VGA2 perteneciente al RX.
 *
 * @param file			Referencia al dispositivo SPI
 * @param reg			Registro para leer la ganancia.
 *
 * @return				Valor de la ganacia.
 */

int getbbGain (int file);

/**
 *  @brief Retorna los nombres de los amplificadores de ganancia.
 *
 * Funcion no implemantada.
 *
 * @param
 *
 * @return
 */

void getGainNames(void);

/**
 *  @brief Imprime el nombre de la ganancia.
 *
 * Imprime por pantalla el nombre de la ganancia que se puede setear.
 * En este caso unicamente es el del amplificador de RX VGA2.
 *
 *	- Valores de ganancias admitidos:
 *		-  0 dB.
 *		-  3 dB.
 *		-  6 dB.
 *		-  9 dB.
 *		- 12 dB.
 *		- 15 dB.
 *		- 18 dB.
 *		- 21 dB.
 *		- 24 dB.
 *		- 27 dB.
 *		- 30 dB.
 * @param
 *
 * @return
 */

void getGainRange(void);

/**
 *  @brief Selecciona el modo de aplicar la ganacia.
 *
 * Funcion no implementada.
 *
 * @param
 *
 * @return
 */

void setGainMode(void);

/**
 *  @brief Retorna los modos posibles de setear la ganancia.
 *
 * Unico modo: Setear ganancia al RXVGA2.
 *
 * @param
 *
 * @return
 */

void getGainMode(void);

#endif /* GAIN_H_ */
