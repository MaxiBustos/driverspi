/*
 * readProyect.h
 *
 *  Created on: Jun 16, 2017
 *      Author: maxi
 */

#ifndef READPROYECT_H_
#define READPROYECT_H_

#include <stdint.h>
#include <unistd.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <getopt.h>
#include <fcntl.h>
#include <sys/ioctl.h>
#include <linux/types.h>
#include <linux/spi/spidev.h>
#include "SPIfunction.h"
#include "parseOpts.h"

/**
 *  @brief Lee valores de un archivo.
 *
 *	En caso de que se quiera cargar una configuracion guardara al transceptor
 *	esta funcion se encraga de leer archivo que se le pase mediante argumento
 *	y setea los registros al chip LMS.
 *
 * @param file		Referencia a dispositivo SPI.
 * @param arch		Nombre del archivo.
 *
 * @return			Estado de la operacion de escritura.
 */

int readProyect (int file, char *nombreArchivo);


#endif /* READPROYECT_H_ */
