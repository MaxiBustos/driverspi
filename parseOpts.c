#include "parseOpts.h"


int regRead   	   = -1; //Var global para el argumento de lectura
int regWrite  	   = -1; //Var global para el argumento de escritura
int bbGain   	   = -1;    //Reg RXVGA2 gain control
float valueWrite   = 0; //Valor que se escribe en registro, usado en spi_write
float centreFreq   = 0;
int antenna		   = 1;
float bandwidth	   = -1;
int flagOpts  	   = 0;
int mode 		   =1; //Mode = 0 -> TX Mode = 1 RX
char *nombreArchivo=0;

int moduleSelect(char * optarg){
	int mode_ =0;

	mode_ = (strcmp(optarg, "tx")== 0) ? 0 :
			(strcmp(optarg, "rx")== 0) ? 1 : 2;

	if(mode_ > 1)
		printf("MODE=FAIL");

	return mode_;
}

void print_usage(const char *prog)
{
	printf("SPI: Driver for the configuration of registers of the chip LMS 6002. \"Version 1.0\"  \n");
	printf("Usage: %s [-DrwvfFTgGKUSaAbBCR]\n", prog);
	puts("Device:\n"
		"  \t-d --device		device to use (default /dev/spidev32766.0)\n"
		" \nRegisters:\n"
	    "  \t-r --read		read register\n"
	    "  \t-w --write		write register\n"
	    "  \t-v --value		value register\n"
		"  \t-R --readFile		load configuration from File\n"
		" \nMode Function:\n"
		"  \t-m --mode		Mode Modulo mode = 0 TX, mode = 1 RX (default RX)\n"
		" \nFrequency Function:\n"
	    "  \t-f --freq		center frequency RX [GHz], for example: 0.75\n"
		"  \t-F --getFreq		get center frequency RX\n"
		"  \t-T --getFreqRange	get frequency range RX\n"
		" \nGain Function:\n"
    	"  \t-g --bbgain		set RXVGA2 gain control {0,3,6,9,...,27,30}[dB]\n"
        "  \t-G --getGain		get RXVGA2 gain control\n"
		"  \t-K --getGainRange	get RXVGA2 gain range\n"
		"  \t-U --getGainName	get RXVGA2 gain name\n"
		"  \t-Y --getGainMode	get RXVGA2 gain mode\n"
		" \nSample Rate Function:\n"
	    "  \t-S --samRate		get Sample Rate 30.72 MHz\n"
		" \nAntenna Function:\n"
		"  \t-a --setAnt		set Antenna Disabled=0, LNA1=1, LNA=2, LNA3=3 for RX, PA1=1 PA2=2 for TX\n"
	    "  \t-A --getAnt		get Antenna selected\n"
		" \nBandwidth Function:\n"
		"  \t-b --setBandw		set bandwidth filter pass low RX\n"
		"  \t-B --getBandw		get bandwidth filter pass low RX\n"
		"  \t-C --getBandRange	get bandwidth range filter pass low RX\n"
		"\t\n\nFull documentation and repository at: <https://bitbucket.org/MaxiBustos/driverspi>\n"
			);
	exit(1);
}

void parse_opts(int argc, char *argv[])
{
	while (1) {
		static const struct option lopts[] = {
			{ "device",   		1, 0, 'd' },
			{ "read",     		1, 0, 'r' },
			{ "write",    		1, 0, 'w' },
			{ "value",    		1, 0, 'v' },
			{ "readFile", 		1, 0, 'R' },
			{ "mode",     		1, 0, 'm' },
			{ "freqRX",    		1, 0, 'f' },
			{ "getFreq",  		0, 0, 'F' },
			{ "getFreqRange",	0, 0, 'T' },
			{ "bbgain",   		1, 0, 'g' },
			{ "getGain",  		0, 0, 'G' },
			{ "getGainRange",	0, 0, 'K' },
			{ "getGainName",	0, 0, 'U' },
			{ "getMode",		0, 0, 'Y' },
			{ "samRate",  		0, 0, 'S' },
			{ "getAnt",   		0, 0, 'A' },
			{ "setBand",  		1, 0, 'b' },
			{ "getBand",  		0, 0, 'B' },
			{ "getBandRange",	1, 0, 'C' },

			{ NULL, 0, 0, 0 },
		};
		int c;

		c = getopt_long(argc, argv, "d:r:w:v:f:Fg:GSAb:Bm:TCKUYR:", lopts, NULL);

		if (c == -1)
			break;

		switch (c) {
		case 'd':
			device = optarg;
			break;
		case 'r':
			regRead = strtol(optarg, NULL, 16);
			break;
		case 'w':
			regWrite = strtol(optarg, NULL, 16);
			break;
		case 'm':
			mode = moduleSelect(optarg);
			break;
		case 'v':
			valueWrite = strtol(optarg, NULL, 16);
			break;
		case 'f':
			centreFreq = atof(optarg);
			break;
		case 'F':
			flagOpts = 1;
			break;
		case 'g':
			bbGain = atoi(optarg);
			break;
		case 'G':
			flagOpts = 2;
			break;
		case 'S':
			flagOpts = 3;
			break;
		case 'A':
			flagOpts = 4;
			break;
		case 'b':
			bandwidth = atof(optarg);
			break;
		case 'B':
			flagOpts = 5;
			break;
		case 'C':
			flagOpts = 6;
			break;
		case 'T':
			flagOpts = 7;
			break;
		case 'K':
			flagOpts = 8;
			break;
		case 'U':
			flagOpts = 9;
			break;
		case 'Y':
			flagOpts = 10;
			break;
		case 'R':
			flagOpts = 11;
			nombreArchivo = (optarg);
			break;
		default:
			print_usage(argv[0]);
			break;
		}
	}
}
