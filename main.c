/**
 * @brief  Los siguientes enlaces contienen la informacion precisa para conocer mas en
 * profundidad el chip LMS6002.
 *
 * LMS6002D Project page:
 *  http://www.limemicro.com/products/LMS6002D.php?sector=default
 *
 * LMS6002D Datasheet:
 *  http://www.limemicro.com/download/LMS6002Dr2-DataSheet-1.2r0.pdf
 *
 * LMS6002D Programming and Calibration Guide:
 *  http://www.limemicro.com/download/LMS6002Dr2-Programming_and_Calibration_Guide-1.1r1.pdf
 *
 * LMS6002D FAQ:
 *  http://www.limemicro.com/download/FAQ_v1.0r10.pdf
 *
 */

#include <stdint.h>
#include <unistd.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <getopt.h>
#include "SPIfunction.h"
#include "CentreFrequency.h"
#include "gain.h"
#include "parseOpts.h"
#include "sampleRates.h"
#include "antenna.h"
#include "bandwidth.h"
#include "readProyect.h"
#include "calibration.h"

int main(int argc, char *argv[]){

	parse_opts(argc, argv);
	int file = spi_init("/dev/spidev32766.0");
	int status = 0;
	int data = 0;
	float retCentreFreq =0;
	float sample =0;

	if (file < 0)	{
		printf("Error");
		exit(1);
	}
    if(regRead >= 0){
		data = spi_read(file, regRead);
		if (status < 0)
			perror("SPI_IOC_MESSAGE");
		printf(" %x", data);
	}
    if(regWrite >=0){
    	status = spi_write(file, regWrite, valueWrite);
		if (status < 0)
			perror("SPI_IOC_MESSAGE");
    }
    if(centreFreq >0){
    	status = setCentreFreq(file, centreFreq, mode);
		if (status>0)
			printf("OK");
		else
			printf("Error");
    	printf(" %f GHz", centreFreq);
    }
    if(bbGain >=0){
    	status = setbbGain(file, bbGain);
    	if (status >0)
    		printf(" %d dB", bbGain);
    	else
    		printf("Error");
    }
    if(bandwidth >= 0){
         	status = setBandwidth(file, bandwidth, mode);
         	printf(" %d Code", status);
         }

    /* Opciones para las funciones GET */
  	switch(flagOpts){
  		case 1:
        	retCentreFreq = getCentreFreq(file, mode);
        	printf(" %f GHz", retCentreFreq);
        	break;
  		case 2:
  			status = getbbGain(file);
				if (status >=0)
					printf(" %d dB", status);
				else
					printf("Error");
			break;
  		case 3:
  			sample = getSampleRate();
			printf(" %f MHz", sample);
			break;
  		case 4:
  			status = getAntenna(file);
			printf(" %d", status);
			break;
  		case 5:
  			status = getBandwidth(file, mode);
			printf(" %d MHz", status);
			break;
  		case 6:
  			getBandwidthRange();
			break;
  		case 7:
  			getFreqRange();
			break;
  		case 8:
  			getGainRange();
			break;
  		case 9:
  			getGainNames();
			break;
  		case 10:
			getGainMode();
			break;
  		case 11:
			readProyect(file, nombreArchivo);
			break;
  		default:
  			break;
  	}

  	//autoCalibration (file);

close(file);
return 0;
}
