#ifndef BANDWIDTH_H_
#define BANDWIDTH_H_

#include <stdint.h>
#include <unistd.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <getopt.h>
#include "SPIfunction.h"

/**
 * @brief Setea el ancho de banda del filtro LPF en RX.
 *
 * Se selecciona el ancho de banda del filtro escribiendo en
 * los bits [5:2] de registro 0x54 del LMS
 *
 * BWC_LPF[3:0]: LPF bandwidth control:
 * code		Bandwidth [MHz]
 * ========================
 * 0000		14 (default)
 * 0001		10
 * 0010		7
 * 0011		6
 * 0100		5
 * 0101		4.375
 * 0110		3.5
 * 0111		3
 * 1000		2.75
 * 1001		2.5
 * 1010		1.92
 * 1011		1.5
 * 1100		1.375
 * 1101		1.25
 * 1110		0.875
 * 1111		0.75
 *
 * @param	file		Referencia al dispositivo SPI.
 * @param   bandwidth	Valor del ancho de banda del filtro.
 * @param	mode		Mode = 0 -> TX, Mode = 1 -> RX
 *
 * @return  status		Estado de la operacion de escritura del registro.
 */

int setBandwidth (int file, int bandwidth, int mode);

/**
 *  @brief Devuelve el camino LNA activo.
 *
 * Funcion que retorna el ancho de banda correspondiente
 * del filtro RXLPF. Leyendo los bits [5:2] de registro 0x54 del LMS.
 * BWC_LPF[3:0]: LPF bandwidth control:
 * code		Bandwidth [MHz]
 * ========================
 * 0000		14 (default)
 * 0001		10
 * 0010		7
 * 0011		6
 * 0100		5
 * 0101		4.375
 * 0110		3.5
 * 0111		3
 * 1000		2.75
 * 1001		2.5
 * 1010		1.92
 * 1011		1.5
 * 1100		1.375
 * 1101		1.25
 * 1110		0.875
 * 1111		0.75
 *
 * @param	file		Referencia al dispositivo SPI.
 * @param	mode		Mode = 0 -> TX, Mode = 1 -> RX
 *
 * @return  status		Estado de la operacion de escritura en registros.
 */

uint32_t getBandwidth (int file, int mode);

/**
 *  @brief Devuelve el rango de ancho de banda del filtro.
 *
 * Ancho de banda en banda base del filtro RXVGA2 0.75 a 14 MHz.
 *
 * @param
 *
 * @return
 */

void getBandwidthRange (void);


#endif /* BANDWIDTH_H_ */
