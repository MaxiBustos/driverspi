#ifndef CALIBRATION_H_
#define CALIBRATION_H_

#include <stdint.h>
#include <unistd.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <getopt.h>
#include <fcntl.h>
#include <sys/ioctl.h>
#include <linux/types.h>
#include <linux/spi/spidev.h>
#include "SPIfunction.h"
#include "CentreFrequency.h"

/**
 *  @brief Calibracion General DC.
 *
 *	Procedimiento para logra la calibracion general.
 *	Diagrama de flujo del documento LMS6002 –
 *	Wide Band Multi Standard Radio Chip - Programming
 *	 and Calibration Guide -la figura 4.1 pag 36.
 *
 *	 \image html imagenes/diagramflujo4-1.png
 *
 * @param file		Referencia a dispositivo SPI.
 *
 * @return			Estado de la operacion de escritura.
 */

int generalDCalibration (int file, int base, int addr);

/**
 *  @brief TX/RX LPF DC Offset Calibration
 *
 *	Procedimiento para logra la calibracion del Offset LPF tanto
 *	para el receptor (mode =0) como transmisor (mode =1).
 *	Diagrama de flujo del documento LMS6002 –
 *	Wide Band Multi Standard Radio Chip - Programming
 *	 and Calibration Guide -la figura 4.3 pag 38.
 *
 *	 \image html imagenes/diagramflujo4-3.png
 *
 * @param file		Referencia a dispositivo SPI.
 * @param mode		MODE=0 rx || MODE=1 tx.
 * @param base		Direccion de memoria base para el modulo a configurar.
 * @param addr		Direccion del modulo para la calibracion
 *
 * @return			Estado de la operacion de escritura.
 */

int lpfDCOffsetCalibration (int file, int mode, int base, int addr);

/**
 *  @brief LPF Bandwidth Tuning
 *
 *	Procedimiento para logra la calibracion de LPF Bandwidth Tuning
 *	Diagrama de flujo del documento LMS6002 –
 *	Wide Band Multi Standard Radio Chip - Programming
 *	 and Calibration Guide -la figura 4.5 pag 40.
 *
 *	 \image html imagenes/diagramfllujo4-5.png
 *
 * @param file		Referencia a dispositivo SPI.
 *
 * @return			Estado de la operacion de escritura.
 */

int lpfBandwidthTuning (int file);

/**
 *  @brief DC Offset Calibration of LPF Tuning Module
 *
 *	Procedimiento para logra la calibracion de DC Offset
 *	Calibration of LPF Tuning Module  *	Diagrama de flujo
 *	del documento LMS6002 – Wide Band Multi Standard
 *	Radio Chip - Programming and Calibration Guide. Fig 4.2
 *	pag 37.
 *
 *  \image html imagenes/diagramflujo4-2.png
 *
 * @param file		Referencia a dispositivo SPI.
 *
 * @return			Estado de la operacion de escritura.
 */

int dcOffsetCalibrationLPFTuningModule (int file);

/**
 *  @brief RXVGA2 DC Offset Calibration
 *
 *	Procedimiento para logra la calibracion de RXVGA2
 *	DC Offset Calibration.	Diagrama de flujo
 *	del documento LMS6002 – Wide Band Multi Standard
 *	Radio Chip - Programming and Calibration Guide. Fig 4.4
 *	pag 39.
 *
 *	\image html imagenes/diagramflujo4-4.png
 *
 * @param file		Referencia a dispositivo SPI.
 *
 * @return			Estado de la operacion de escritura.
 */

int rxVGA2DCOffsetCalibration (int file);

/**
 *  @brief	Auto Calibration
 *
 * - La siguiente secuencia es recomendada para la auto calibracion :
 * 		-# DC offset cancellation of the LPF tuning module, Figura 4.2.
 * 		-# LPF bandwidth tuning, Figura 4.5.
 * 		-# DC offset cancellation of the TXLPF, Figura 4.3.
 * 		-# DC offset cancellation of the RXLPF, Figura 4.3.
 * 		-# DC offset cancellation of the RXVGA2, Figura 4.4.
 *
 *	Tenga en cuenta que, al ejecutar los procedimientos de calibración
 *	de DC, tanto TX como RX no deben estar habilitados.
 *
 * @param file		Referencia a dispositivo SPI.
 *
 * @return			Estado de la operacion de escritura.
 */

int autoCalibration (int file);

#endif /* CALIBRATION_H_ */
