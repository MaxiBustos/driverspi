/*
 * parseOpts.h
 *
 *  Created on: Jun 12, 2017
 *      Author: maxi
 */

#ifndef PARSEOPTS_H_
#define PARSEOPTS_H_

#include <stdint.h>
#include <unistd.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <getopt.h>

char *device;
char *nombreArchivo;
int regRead;
int regWrite;
int bbGain;
int mask;
float valueWrite;
float centreFreq;
int antenna;
float bandwidth;
int flagOpts;
int mode; //Mode = 0 -> TX Mode = 1 RX



/**
 *  @brief Imprime metodos del driver.
 *
 * @param prog	Nombre del Software.
 *
 * @return
 */

void print_usage(const char *prog);

/**
 *  @brief Parsea las opciones introducidas.
 *
 * @param argc 		cantidad de parámetros contando el nombre.
 * @param argv      arreglo que contiene todos los parámetros recibidos.
 *
 * @return				Estado de la operacion de escritura.
 */

void parse_opts(int argc, char *argv[]);

#endif /* PARSEOPTS_H_ */
